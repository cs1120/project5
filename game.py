"""
game.py
"""

from player import Player
from gamestate import GameState

class Game:
    """
    A Game represents a turn-based game with
    public and complete state (perfect information).
    """
    
    def __init__(self, players, state):
        """
        Initializes a new Game with the given
        players and initial state.
        """
        
        # all players must be of type Player
        assert all([isinstance(player, Player) for player in players])
        self._players = players

        # the state must be a GameState
        assert isinstance(state, GameState)
        self._state = state
        
    def play(self, show_moves = False):
        """
        Play the game.  Each players gets a turn in order,
        until the game is over.

        Returns a pair: (winner, list of moves)
        where each move in moves is a list of the moves
        for each player for that round.
        """

        turn = 0
        moves = []
        all_pass = False
        while not all_pass:
            turn += 1
            moves.append([])
            all_pass = True
            
            for player in self._players:
                if self._state.game_over():
                    break
    
                move = player.turn(self._state)
                moves[-1].append(move)

                if move:
                    assert self._state.legal_move(player, move)
                    self._state.make_move(player, move)
                    all_pass = False

                if show_moves:
                    print("State after " + player.mark() + " moves " \
                          + str(move) if move else "[pass]")
                    print(str(self._state))
                    
            
        return (self._state.outcome(), moves)

        
                    
        
