"""
humanplayer.py
"""

from player import Player

class HumanPlayer(Player):
    """
    A player that requests input from a human (or whatever might
    be entering text into the console!) to select a move.
    """
    def __init__(self, name, mark, movetype):
        super().__init__(name, mark)
        self._movetype = movetype
                 
    def turn(self, state):
        # print("Current state: \n" + str(state))
        while True:
            enter = input("Your move: ")
            try:
                if enter == "":
                    return None
                move = self._movetype.parse(enter)
                if not state.valid_position(move):
                    print("Invalid position! Try again.")
                elif not state.legal_move(self, move):
                    print("Illegal move!  Try again.")
                else:
                    return move
            except ValueError as e:
                print("Cannot interpret move: " + str(e))
