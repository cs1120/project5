"""
player.py
"""

class Player:
    """
    A generic game player.
    """
    
    def __init__(self, name, mark):
        self._name = name
        self._mark = mark

    def mark(self):
        return self._mark
    
    def name(self):
        """
        Returns the name of this player.
        """
        return self._name

    def turn(self, state):
        raise RuntimeAssertion("Generic player doesn't know how to play!")
    
    def __str__(self):
        return "Generic Player: " + self.name()
    
